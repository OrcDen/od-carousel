const merge = require('webpack-merge');
const common = require('./common/webpack.common');
const path = require('path');
const nodeExternals = require('webpack-node-externals');

module.exports = merge(common, {
    mode: 'production',
    entry: {
        index: './src/main.js'
    },
    output: {
        path: path.resolve(__dirname, '../dist'),
        chunkFilename: '[name].chunk.js'
    },
    externals: [nodeExternals()]
});