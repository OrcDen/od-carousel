const merge = require('webpack-merge');
const common = require('./common/webpack.common');
const devCommon = require('./common/webpack.dev.common');

module.exports = merge(common, devCommon, {
    devtool: 'inline-source-map',
    devServer: {
        open: true,
        contentBase: './demo/build',
        port: 8080
    }
});