const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: {
        'index' : './demo/index.js'
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './demo/template.html',
            title: 'OD Carousel Demo',
            meta: {
                viewport: 'width=device-width, initial-scale=1'
            }
        }),
        new CopyWebpackPlugin([
            {
                context: 'node_modules/@webcomponents/webcomponentsjs',
                from: '**/*.js',
                to: 'webcomponents'
            }
        ])
    ],
    output: {
		path: __dirname + "/demo/build"
	},
    mode: "development"
};