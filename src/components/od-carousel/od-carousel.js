const template = document.createElement( 'template' );
template.innerHTML = `
    <style>
        :host {
            display: block;
            height: inherit;

            border: solid 1px black;
            box-sizing: border-box;
        }
    
        #content::slotted(*) {
            display: flex;
            margin: auto;
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            justify-content: center;
        }

        #content::slotted(*:not([selected]):not([transitioning])) {
            display: none !important;
        }

        :host([hide-buttons]) button {
            display: none;
        }
    
        button {
            position: absolute;
            top: calc(50% - 20px);
            padding: 0;
            line-height: 40px;
            border: none;
            background: none;
            color: #DDD;
            font-size: 40px;
            font-weight: bold;
            opacity: 0.7;
        }
    
        button:hover,
        button:focus {
            opacity: 1;
            outline: none;
        }
    
        #content-container {
            display: block;
            overflow: hidden;
            position: relative;
            height: 100%;
        }
    
        #prev-btn {
            left: 12px;
        }
    
        #next-btn {
            right: 12px;
        }
    
        button[disabled] {
            opacity: 0.4;
        }
    </style>

    <div id="content-container" part='container'>
        <slot id='content'></slot>
        <button id="prev-btn" part='prev-button'>&#x276E;</button> 
        <button id="next-btn" part='next-button'>&#x276F;</button>
    </div>
`;

class OdCarousel extends HTMLElement {

    constructor() {
        super();
        this.attachShadow( { mode: 'open' } );
        this.shadowRoot.appendChild( template.content.cloneNode( true ) );
        
        this.__selected = null;
        this.__timer = null;
        this._items = [];
    }

    connectedCallback() {
        // set default timer value to 0.  failing test framework
        if( !this.hasAttribute( "transition-timer" ) ){ 
            this.setAttribute( "transition-timer", 0 );
        }
        this._upgradeProperty( "transitionTimer" );
        this._upgradeProperty( "randomStart" );
        this._upgradeProperty( "hideButtons" );

        this.carousel = this.shadowRoot.querySelector( "#content-container" );
        this.prevBtn = this.shadowRoot.querySelector( "#prev-btn" );
        this.nextBtn = this.shadowRoot.querySelector( "#next-btn" );

        this.nextBtn.addEventListener( "click", () => { this.next(); } );
        this.prevBtn.addEventListener( "click", () => { this.previous(); } );
        this.addEventListener( "transitionend", () => { this._resetChildrenStyles(); } );

        this._getItems();
        this._resetSelected();
        this._timer = this.transitionTimer;

        let slot = this.shadowRoot.querySelector( '#content' );
        slot.addEventListener( 'slotchange', ( e ) => {
            this._getItems();
        } )
    }

    disconnectedCallback() {}

    _upgradeProperty( prop ) {
        if( Object.prototype.hasOwnProperty.call( this, prop ) ) {
            var value = this[prop];
            delete this[prop];
            this[prop] = value;
        }
    }

    static get observedAttributes() {
        return [ "transition-timer", "random-start", "hide-buttons" ];
    }

    attributeChangedCallback( attrName, oldValue, newValue ) {
        switch ( attrName ) {
            case "transition-timer": {  
                if( !this._validateTransitionTimer( newValue ) ) {
                    this.setAttribute( "transition-timer", oldValue );
                }
                break;
            }
            case "random-start": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setRandomStartAttribute( oldValue );
                    break
                }
                if( newValue !== oldValue ){
                    this._setRandomStartAttribute( newValue );
                    break;
                }
            } case "hide-buttons": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setHideButtonsAttribute( oldValue );
                    break;
                }
                if ( newValue !== oldValue ) {
                    this._setHideButtonsAttribute( newValue );
                    break;
                }
            }
        }
    }

    _validateBoolean( newValue ){
        return (
            typeof newValue === "boolean" || newValue === "true" || newValue === "false"
        );
    }

    _setBooleanAttribute( name, newV ) {
        if ( newV !== "" && ( !newV || newV === "false" ) ) {
            this.removeAttribute( name );
        } else {
            this.setAttribute( name, true );
        }
    }

    get transitionTimer() {
        return parseInt( this.getAttribute( "transition-timer" ) );
    }

    set transitionTimer( timerValue ) {
        if( typeof timerValue !== "number" || isNaN( timerValue ) ) {
            return;
        }
        if( this._validateTransitionTimer( timerValue ) ) {
            this.setAttribute("transition-timer", timerValue);
        }
    }

    _validateTransitionTimer( newValue ){
        let temp = parseInt( newValue );
        return !isNaN( temp ) && temp >= 0;
    }

    get randomStart() {
        return this.hasAttribute( "random-start" );
    }

    set randomStart( value ) {
        if ( typeof value !== "boolean" ) { 
            return;          
        }
        this._setRandomStartAttribute( value );
    }

    _setRandomStartAttribute( newV ){
        this._setBooleanAttribute( 'random-start', newV );
    }

    get hideButtons() {
        return this.hasAttribute( "hide-buttons" );
    }

    set hideButtons( value ) {
        if ( typeof value !== "boolean" ) {
            return;
        }
        this._setHideButtonsAttribute( value );
    }

    _setHideButtonsAttribute( newV ){
        this._setBooleanAttribute( 'hide-buttons', newV );
    }

    previous() {
        if ( this._items.length < 2 ) {
            return;
        }
        var elem = undefined;
        if ( this._selected === this.firstElementChild ) {
            elem = this.lastElementChild;
        } else {
            elem = this._selected.previousElementSibling;
        }
        if ( this._selected && elem ) {
            // Setup transition start state
            const oldSelected = this._selected;
            this._translateX( oldSelected, 0 );
            this._translateX( elem, -this.offsetWidth );

            // Start the transition\            
            oldSelected.setAttribute( 'transitioning', true )
            this._selected = elem;
            this._translateX( oldSelected, this.offsetWidth, true );
            this._translateX( elem, 0, true );
        }
    }

    next() {
        if ( this._items.length < 2 ) {
            return;
        }
        var elem = undefined;
        if ( this._selected === this.lastElementChild ) {
            elem = this.firstElementChild;
        } else {
            elem = this._selected.nextElementSibling;
        }
        if ( this._selected && elem ) {
            // Setup transition start state
            const oldSelected = this._selected;
            this._translateX( oldSelected, 0 );
            this._translateX( elem, this.offsetWidth );

            // Start the transition
            oldSelected.setAttribute( 'transitioning', true )
            this._selected = elem;
            this._translateX( oldSelected, -this.offsetWidth, true );
            this._translateX( elem, 0, true );
        }
    }

    //internals
    get _timer() {
        return this.__timer;
    }

    set _timer( value ) {
        if( this._timer ) {
            clearInterval( this._timer );
        }
        var parsed = parseInt( value );
        if( !parsed || isNaN( parsed ) ) {
            return;
        }
        this.__timer = setInterval( () => {
            this.next();
        }, parsed * 1000 );
    }

    get _selected() {
        return this.__selected;
    }

    set _selected( element ) {
        if ( this._selected ) {
            this._selected.removeAttribute( "selected" );
            this._dispatchAdEnd( this._selected );
        }
        if ( element ) {
            element.setAttribute( "selected", true );
            this._dispatchAdView( element );
        }
        this.__selected = element;
    }

    get _viewHeight() {
        return this.offsetHeight;
    }

    get _viewWidth() {
        return this.offsetWidth;
    }

    _resetSelected() {
        if ( !this._selected || this._selected.parentElement !== this ) {
            if ( this.randomStart ) {
                var children = this.children;
                var maxIndex = children.length;
                if ( !maxIndex || maxIndex === 0 ) {
                    return;
                }
                var random = this._getRandomArbitrary( 0, maxIndex );
                this._selected = children[ random ];
            } else {
                this._selected = this.firstElementChild;
            }
        }
    }

    _getItems() {
        var temp = this.shadowRoot
            .querySelector( "#content" )
            .assignedNodes( {
                flatten: true
            } )
            .filter( n => n.nodeType === Node.ELEMENT_NODE );
        if ( temp.length < 1 ) {
            return;
        }
        this._items = temp;
    }    

    _getRandomArbitrary( min, max ) {
        return Math.round( Math.random() * ( max - min ) + min );
    }  

    _dispatchAdView( selected ) {
        this.dispatchEvent( new CustomEvent( "adView",
            {
                composed: true,
                bubbles: true,
                'detail': {
                    'id': selected.id,
                    'classes': selected.classList.toString(),
                    'url': selected.href
                }
            }
        ) );
    }

    _dispatchAdEnd( oldSelected ) {
        this.dispatchEvent( new CustomEvent( "adViewEnd",
            {
                composed: true,
                bubbles: true,
                'detail': {
                    'id': oldSelected.id,
                    'classes': oldSelected.classList.toString(),
                    'url': oldSelected.href
                }
            }
        ) );
    }

    _translateX( elem, x, transition ) {
        elem.style.transition = transition ? "transform " + 600 / 1000 + "s ease-in-out" : "";
        elem.style.transform = 'translate3d(' + x + 'px, 0, 0)';
    }

    _resetChildrenStyles() {
        for ( var i in this._items ) {
            var item = this._items[ i ];
            item.style.transition = '';
            item.style.transform = '';
            item.removeAttribute( 'transitioning' );
        }
    }
}

customElements.define( 'od-carousel', OdCarousel );