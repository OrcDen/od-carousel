import { Selector, ClientFunction } from "testcafe";
import { TestCafeComponentTestSuite } from '../../../node_modules/@orcden/od-component-test-suite';
import _config from './config';

const testSuite = new TestCafeComponentTestSuite( _config );
testSuite.runAllTests();

const model = testSuite.model;
const odCarousel = model.component;

fixture( 'OD-Carousel Custom Tests' ).page( 'http://localhost:8079' );

//Most of these below are covered in the suite

test( 'Unit Tests - Attributes - transition-timer - negative value update', async t => {
    await t
        .eval( () => { document.querySelector( '#od-carousel' ).transitionTimer = -10 } );

    await t
        .expect( odCarousel.withAttribute( 'transition-timer', '0' ).exists ).ok( 'Attribute "transition-timer" uses default when negative value is set' );
} );