export default {
    testServer: 'http://localhost:8079',
    name: 'OD Carousel',
    tag: 'od-carousel',
    componentSelector: '#od-carousel',
    hasShadow: true,
    shadowSelector: '#content-container',
    properties: [
        {
            name: 'randomStart',
            type: Boolean,
            default: false,
            attributeName: 'random-start'
        },
        {
            name: 'hideButtons',
            type: Boolean,
            default: false,
            attributeName: 'hide-buttons'
        },
        {
            name: 'transitionTimer',
            type: Number,
            default: 0,
            validValue: 100,
            attributeName: 'transition-timer'
        }
    ]
}