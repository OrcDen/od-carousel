# <od-carousel>

> A component to provided a rotating gallery of content

`<od-carousel>` is a simple component that provides a framework for nesting content that can be rotated through manually or automatically.

## Installation
- Install with [npm](https://www.npmjs.com/)

```
npm i @orcden/od-carousel
```
## Usage
```
import '@orcden/od-carousel';
```
```
<od-carousel id="od-carousel" style="height: 500px;" transition-timer='5'>
    <img src="https://images.unsplash.com/photo-1569409611407-50eee9f59dfe?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1389&q=80">
    <img src="https://images.unsplash.com/photo-1567815883115-bcf719bdc8ad?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1351&q=80">
    <img src="https://images.unsplash.com/photo-1568121581570-a30e94219113?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80">
    <img src="https://images.unsplash.com/photo-1568387188834-ff83d00b9915?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1267&q=80">
    <img src="https://images.unsplash.com/photo-1550728193-be87c574be86?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80">
</od-carousel>
```
    
## Attributes
| Attribute | Type | Default | Description                                                                             |
|-----------|---------|---------|-----------------------------------------------------------------------------------------|
| `transition-timer`  | Number  | 0 | When this is not zero the carousel will automatically transition its content  |
| `random-start`  | Boolean | false   | When this is set the carousel will choose a random child piece of content to display first. Normally used in conjuction with a transition-timer. |
| `hide-buttons`  | Boolean | false | Set this to hide the buttons from the carousel and disable user input.  Normally used in conjuction with a transition-timer.|

## Properties
| Attribute | Type | Default | Description                                                                             |
|-----------|---------|---------|-----------------------------------------------------------------------------------------|
| `transitionTimer`  | Number  | 0 | When this is not zero the carousel will automatically transition its content  |
| `randomStart`  | Boolean | false   | When this is set the carousel will choose a random child piece of content to display first. Normally used in conjuction with a transition-timer. |
| `hideButtons`  | Boolean | false | Set this to hide the buttons from the carousel and disable user input.  Normally used in conjuction with a transition-timer.|

## Functions
| Name | Parameters | Description                                  |
|-----------|------|-----------------------------------------------|
| `previous`   | None | transitions the carousel to display the previous piece of child content in the DOM |
| `next`   | None | transitions the carousel to display the next piece of child content in the DOM |

## Styling
- CSS variables are available to alter the default styling provided

| Shadow Parts     | Description           |
|------------------|-----------------------|
| container           | The div used to house the child content |
| prev-button           | The button that when clicked transitions to the previous piece  of content |
| next-button           | The button that when clicked transitions to the next piece  of content |

## Development
### Run development server and show demo

```
npm run demo
```

### Run linter

```
npm run lint
```

### Fix linter errors

```
npm run fix
```

### Run tests

```
npm run test
```

### Build for production

```
npm run build
```